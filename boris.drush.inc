<?php
/**
 * @file Drush boris commands
 */

/**
 * Implementation of hook_drush_help().
 */
function boris_drush_help($section) {
  switch ($section) {
    case 'meta:boris:title':
      return dt('boris commands');
    case 'meta:boris:summary':
      return dt('Commands for using Drupal from within a boris interactive shell.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function boris_drush_command() {
  $items['boris-console'] = array(
    'description' => 'Run an interactive shell in the current Drupal environment. Requires a working boris (https://github.com/d11wtq/boris).',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'aliases' => array('console'),
    'options' => array(
      'boris' => 'Specify the path to the boris executable. If not provided, `which boris` will be used.',
    ),
    'examples' => array(
      'drush console' => 'Start a console using default options. Assumes a configured Drupal root can be located by drush and the boris executable can be located with `which boris`.',
      'drush @local console' => 'Start a console using a drush site alias. This allows you to start a console without first changing to the appropriate directory or specifying a root and/or uri parameter for drush.',
      'drush console --boris=/usr/local/bin/boris' => 'Specify where to find boris, in case it is not in your $PATH variable, or not the first one found.',
    ),
  );
  return $items;
}

/**
 * Validation callback for `drush boris-console`
 */
function drush_boris_console_validate() {
  $boris = drush_get_option('boris');

  if (empty($boris)) {
    $status = drush_shell_exec("which boris 2>&1");
    if ($status === FALSE) {
      return drush_set_error('BORIS_NOT_FOUND', dt('Cannot find boris executable.'));
    }
    $output = drush_shell_exec_output();
    $boris = $output[0];
  }

  if (!file_exists($boris)) {
    return drush_set_error('BORIS_NOT_FOUND', dt('Cannot find boris executable.'));
  }

  if (!is_executable($boris)) {
    return drush_set_error('BORIS_NOT_EXECUTABLE', dt('The specified boris file is not executable.'));
  }

  drush_set_option('boris', $boris);
}

/**
 * Command callback for `drush boris-console`
 *
 * Start boris in the Drupal root with the version specific startup script.
 */
function drush_boris_console() {
  $command = drush_get_command();
  $include = $command['path'] . '/includes/console_' . drush_drupal_major_version() . '.inc';

  // Add some required items to the $_SERVER array, which will be used as the
  // startup environment for boris.
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
  $_SERVER['SCRIPT_NAME'] = '/index.php';
  $_SERVER['REQUEST_URI'] = '/';
  $_SERVER['HTTP_HOST'] = parse_url(drush_get_context('DRUSH_URI'), PHP_URL_HOST);

  $root = drush_get_context('DRUSH_DRUPAL_ROOT');
  $boris = drush_get_option('boris');

  $command = join(' ', array(
    $boris,
    drush_escapeshellarg($include),
  ));

  if (drush_get_context('DRUSH_VERBOSE')) {
    drush_print('Executing: ' . $command);
    drush_print_r($_SERVER);
  }

  proc_close(proc_open($command, array(0 => STDIN, 1 => STDOUT, 2 => STDERR), $pipes, $root, $_SERVER));
}
